 /* This class is a data access object to provide an interface 
  * to the SQLite database via SQL statements
  * @author Aliya Tulyakova, ID 16035926
  * @version 15.11.2016
  */
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.Connection;
 import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;
 
 public class DoctorDAO {
	    /*Create a Connection object, Statement object, ResultSet object and sets to null
	     * @param c - Connection object
	     * @param s - Statement object
	     * @param r - ResultSet object
	     * */
		Connection c = null;
		Statement s= null;
		ResultSet r = null;
		
		/**Get Database Connection
		 *  
		 * @return Statement Object
		 * @throws ClassNotFoundException 
		 */
	   public Statement getConnection()  {

			try {
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:doctordb.sqlite"); //get doctordb.sqlite database connection 
				s = c.createStatement();	
	}
	catch (SQLException e) {
		e.printStackTrace();
	}
	catch (ClassNotFoundException c){
		c.printStackTrace();
	}
	return s;
}
 /*
  * Close any open database connection
  * @throws SQLException
  */
	public void closeConnection(){
		try {
			if(s!=null){
				s.close();
			}
			if(c!=null){
				c.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * Retrieve all Employees, create an ArrayList, execute an SQLQuery, get DB Connection 
	 * @return Returns ArrayList doctorArray 
	 * @throws SQLException
	 */
	public ArrayList<Doctor> selectAllEmployees(){
		ArrayList<Doctor> doctorArray=new ArrayList<Doctor>();
	try{
		/* Executes an sql query*/
		String sql="select * from doctors;";
		ResultSet rs=getConnection().executeQuery(sql);
		
		if(rs!=null) {
			while(rs.next()) {
				Doctor doctor=new Doctor("","","","","","","","","");
			try{
			doctor.setName(rs.getString("Name"));
		    doctor.setGender(rs.getString("Gender"));
		    doctor.setDob(rs.getString("Dob"));
		    doctor.setAddress(rs.getString("Address"));
		    doctor.setPostcode(rs.getString("Postcode"));
		    doctor.setNIN(rs.getString("NIN"));
			doctor.setSpecialism(rs.getString("Specialism"));
			doctor.setStartDate(rs.getString("StartDate"));
			doctor.setEmail(rs.getString("Email"));
			
			} 
			catch(SQLException s){
				s.printStackTrace(); //Inform user of any SQL errors
			}
			doctorArray.add(doctor);
			}
			rs.close(); //close ResultSet object
		}
	} 
	catch (SQLException s){
		
	}
	closeConnection(); //close Connection
	return doctorArray;
	}
	
	/*
	 * Retrieve name, specialism, create an ArrayList, execute an SQLQuery, get DB Connection 
	 * 
	 * @return Returns ArrayList doctorInfo 
	 * @throws SQLException
	 */
	public ArrayList<Doctor> selectInfo(){
		ArrayList<Doctor> doctorInfo=new ArrayList<Doctor>();
	try{
		//Create and execute an SQL Statement
		String sql="select name, specialism from doctors;";
		ResultSet rs=getConnection().executeQuery(sql);
		
		if(rs!=null) {
			
			//Display the SQL Results
			while(rs.next()) {
				Doctor doctor=new Doctor("","","","","","","","","");
			try{
			doctor.setName(rs.getString("Name"));
		
			doctor.setSpecialism(rs.getString("Specialism"));
			
			} 
			catch(SQLException s){
				s.printStackTrace();
			}
			doctorInfo.add(doctor);
			}
			rs.close(); //close ResultSet 
		}
	} 
	catch (SQLException s){
		
	}
	closeConnection(); //close Connection
	return doctorInfo;
	}
	
	/*
	 * Retrieve name, gender, dob, address, postcode, NIN, startdate, email from doctors, create an ArrayList, execute an SQLQuery, get DB Connection  
	 * @return Returns ArrayList docArray
	 * @throws SQLException
	 */
	
	public ArrayList<Doctor> selectMoreInfo(){
		ArrayList<Doctor> docArray=new ArrayList<Doctor>();
	try{
		String sql="select name, gender, dob, address, postcode, NIN, startdate, email  from doctors;";
		ResultSet rs=getConnection().executeQuery(sql);
		
		if(rs!=null) {
			//Display SQL results
			while(rs.next()) {
				Doctor doctor=new Doctor("","","","","","","","","");
			try{
			doctor.setName(rs.getString("Name"));
			doctor.setGender(rs.getString("Gender"));
			doctor.setDob(rs.getString("Dob"));
			doctor.setAddress(rs.getString("Address"));
			doctor.setPostcode(rs.getString("Postcode"));
			doctor.setNIN(rs.getString("NIN"));
			
			doctor.setStartDate(rs.getString("StartDate"));
			doctor.setEmail(rs.getString("Email"));
			} 
			catch(SQLException s){
				s.printStackTrace();
			}
			docArray.add(doctor);
			}
			rs.close(); //close ResultSet 
		}
	} 
	catch (SQLException s){
		
	}
	closeConnection(); //close Connection
	return docArray;
	}
	
	
/*
 * Retrieve an Employee by Name
 * @return Returns an employee whose name is 'Tony Abbott'
 * @throws Exception
 *
*/
	
	public void selectEmployeeByName() throws SQLException {
	
		try{
			 
			 Class.forName("org.sqlite.JDBC"); //Load the driver
			 c=DriverManager.getConnection("jdbc:sqlite:doctordb.sqlite"); //get doctordb connection
			 /* Creates statement*/
			 s=c.createStatement();
			 System.out.println("Retrieve an employee by name:");
			//System.out.println("Enter employee's name to search: ");
	        //Scanner scan=new Scanner(System.in);
	        //String searchname=scan.nextLine();
	          String query="SELECT * FROM DOCTORS WHERE NAME='Tony Abbott'";
	        PreparedStatement pstmt=c.prepareStatement(query);
	        //pstmt.setString(2, searchname);
	        r=pstmt.executeQuery();
	     	 while (r.next()){
	     		 //Display the SQL Result
	     		int id=r.getInt("ID");
	   		 String name=r.getString("Name");
	   		 String gender=r.getString("Gender");
	   		 String dob=r.getString("Dob");
	   		 String address=r.getString("Address");
	   		 String postcode=r.getString("Postcode");
	   		 String nin=r.getString("NIN");
	   		 String specialism=r.getString("Specialism");
	   		 String startdate=r.getString("StartDate");
	   		 String email=r.getString("Email");
	   		 System.out.println("ID "+id);
	   		 System.out.print(" Name "+name);
	   		 System.out.print(" Gender "+gender);
	   		 System.out.print(" Dob "+dob);
	   		 System.out.print(" Address "+address);
	   		 System.out.print(" Postcode "+postcode);
	   		 System.out.print(" NIN "+nin);
	   		 System.out.print(" Specialism "+specialism);
	   		 System.out.print(" StartDate "+startdate);
	   		 System.out.print(" Email "+email);
	   		 System.out.println();
	   		 
	   	 }
	     	  
	   	 r.close(); //close ResultSet
	   	 s.close(); //close Statement
	   	 c.close(); //close Connection
	    } catch (Exception e){
	   	 System.err.println(e.getClass().getName()+":"+e.getMessage());
	   	 System.exit(0);
	    }
	   	 System.out.println("Read operation successfully done");
	    }
	  
	    
	     	     
/*
 * Insert Doctor into database
 * 
 * @param c Doctor Object
 * @return True if inserted
 * @throws SQLException Any error message thrown
 */
	
 public boolean insertEmployee (Doctor c) throws SQLException {
	boolean b=false;
	try {
	String sql="insert into doctors (Name, Gender, Dob, Address, Postcode, NIN, Specialism, StartDate, Email) "
			+ "values (\"" +c.getName()+"\","+"\""+c.getGender()+"\","+"\""+c.getDob()+"\","+"\""+c.getAddress()+"\","+"\""+c.getPostcode()+"\","+"\""+c.getNIN()
			+"\","+"\""+c.getSpecialism()+"\","+"\""+c.getStartDate()+"\","+"\""+c.getEmail()+"\")";
	
	b=getConnection().execute(sql);
	closeConnection();
	}
	catch (SQLException s){
		throw new SQLException("Employee Not Added");
	} return b;
 }
 
 /*
  * Delete Doctor by ID by entering an ID in a console
  * @param id - ID Number of a doctor
  * @throws Exception
  * 
  */
 
 public void deleteDoctorById () throws SQLException {
	 
	 try{
		 
		Class.forName("org.sqlite.JDBC"); //load the driver
		 c=DriverManager.getConnection("jdbc:sqlite:doctordb.sqlite"); // get doctordb connection
		 c.setAutoCommit(false);
		 System.out.println("Delete operation - database successfully opened");
		 
		 System.out.println(" Please enter doctor's ID to delete"); //prompt for input
   	  Scanner sc=new Scanner(System.in); //create Scanner object 
   	   int id=sc.nextInt(); //input method called
   	   /*Create a Prepared Statement object from a Connection object*/
   	PreparedStatement pstmt=c.prepareStatement("DELETE FROM DOCTORS WHERE ID= ?"); 
   	pstmt.setInt(1, id);
	    pstmt.executeUpdate();
	     c.commit();
	     pstmt.close(); //close PreparedStatement
	     c.close(); //close Connection
	 } catch (Exception e){
		 System.err.println(e.getMessage());
		 System.exit(0);
	 } System.out.println("Delete operation successfully done");
 }
 
 /*
  * Retrieve all employees again (update) after deleting an employee
  * @throws Exception
  */
 
 public void retrieveafterdel () throws SQLException{
	 
	 try{
	 Class.forName("org.sqlite.JDBC"); //load the driver
	 c=DriverManager.getConnection("jdbc:sqlite:doctordb.sqlite"); //get doctordb connection
	 c.setAutoCommit(false);
	 System.out.println("Read operation - Database successfully opened");
	 /* Create and execute an SQL statement*/
	 s=c.createStatement();
	 ResultSet rs=s.executeQuery("SELECT * FROM DOCTORS;");
	 /*Display the SQL results*/
	 while (rs.next()) {
		 int id=rs.getInt("ID");
		 String name=rs.getString("Name");
		 String gender=rs.getString("Gender");
		 String dob=rs.getString("Dob");
		 String address=rs.getString("Address");
		 String postcode=rs.getString("Postcode");
		 String nin=rs.getString("NIN");
		 String specialism=rs.getString("Specialism");
		 String startdate=rs.getString("StartDate");
		 String email=rs.getString("Email");
		 System.out.println("ID "+id);
		 System.out.print(" Name "+name);
		 System.out.print(" Gender "+gender);
		 System.out.print(" Dob "+dob);
		 System.out.print(" Address "+address);
		 System.out.print(" Postcode "+postcode);
		 System.out.print(" NIN "+nin);
		 System.out.print(" Specialism "+specialism);
		 System.out.print(" StartDate "+startdate);
		 System.out.print(" Email "+email);
		 System.out.println();
		 
	 }
	 rs.close(); //close ResultSet
	 s.close(); //close Statement
	 c.close(); //close Connection
 } catch (Exception e){
	 System.err.println(e.getClass().getName()+":"+e.getMessage());
	 System.exit(0);
 }
	 System.out.println("Read operation successfully done");
 }
 }