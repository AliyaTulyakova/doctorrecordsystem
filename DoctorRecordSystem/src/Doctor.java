/*
 * This class is a subclass of Person
 * @author Aliya Tulyakova, ID 16035926
 * @version 27.10.2016
 */
public class Doctor extends Person {
	
	
	/*Create unique attributes to the subclass*/
	private String Specialism;
	private String StartDate;
	private String Email;
	
	/*Constructor Doctor Name, Gender, Dob, Address, Postcode, NIN from the Person class using SUPER and Specialism, StartDate, Email to to parameters in the constructor
	 *@param Name - describes a name of a doctor
		 * @param Gender - describes a gender of a doctor
		 * @param Dob - describes a date of birth of a doctor
		 * @param Address - describes an address of a doctor
		 * @param Postcode - describes a postcode of a person
		 * @param NIN - describes a National Insurance Number of a doctor
		 * @param Specialism - describes a Specialism of a doctor
		 * @param StartDate - describes a StartDate of a doctor
		 * @param Email - describes an Email of a doctor
	 * */
    public Doctor(String Name, String Gender, String Dob, String Address, String Postcode, String NIN, String Specialism, String StartDate, String Email){
    	super(Name, Gender, Dob, Address, Postcode, NIN);
    	this.Specialism=Specialism; //Specialism is specific to the Doctor class
    	this.StartDate=StartDate; //StartDare is specific to the Doctor class
    	this.Email=Email; //Email is specific to the Doctor class
    }
	/*Create unique methods to the subclass*/
    /*Read the specialism of a doctor
     * @return Returns the value of a doctor's specialism
     * */
	public String getSpecialism() { 
		return Specialism; 
	}
	/*
	 * The method allows to write a Specialism
	 */
	public void setSpecialism(String Specialism) { 
		this.Specialism = Specialism;
	}
	/*Read the date of the first working day of a doctor
     * @return Returns the value of a doctor's date of the first working day
     * */
	public String getStartDate() { 
		return StartDate; 
	}
	/*
	 * The method allows to write a StartDate 
	 */
	public void setStartDate(String StartDate) { 
		this.StartDate = StartDate;
	}
	/*Read the email of a doctor
     * @return Returns the value of a doctor's email
     * */
	public String getEmail() { 
		return Email; 
	}
	/*
	 * The method allows to write an Email
	 */
	public void setEmail(String Email) { 
		this.Email = Email;
	}
	
	/*@return This method gets the doctor's information*/
	
	public String toString(){
		return "Name: "+super.getName()+" Gender: "+super.getGender()+" Dob: "+super.getDob()+" Address: "+super.getAddress()+" Postcode: "+super.getPostcode()+" NIN: "+super.getNIN()+" Specialism: "+this.Specialism+ " StartDate "+this.StartDate+" Email "+this.Email;
	}

}
