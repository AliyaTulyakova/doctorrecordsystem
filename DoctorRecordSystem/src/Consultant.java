/* This is Consultants class that extends Doctor class (a subclass of Doctor class)
 * @author Aliya Tulyakova, ID 16035926
 * @version 15.12.2016
 */
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class Consultant extends Doctor {
    
	/*Create unique attributes to the subclass*/
	private String title;
	
	/*Constructor Consultant Name, Gender, Dob, Address, Postcode, NIN from the Person class, Specialism, StartDate, Email from the Doctor class using SUPER and Title to to parameters in the constructor
	 *@param Name - describes a name of a consultant
		 * @param Gender - describes a gender of a consultant
		 * @param Dob - describes a date of birth of a consultant
		 * @param Address - describes an address of a consultant
		 * @param Postcode - describes a postcode of a consultant
		 * @param NIN - describes a National Insurance Number of a consultant
		 * @param Specialism - describes a Specialism of a consultant
		 * @param StartDate - describes a StartDate of a consultant
		 * @param Email - describes an Email of a consultant 
		 * @param Title - describes a Title of a consultant
	 * */
	public Consultant(String name,String gender, String dob, String address,
			String postcode, String nin, String specialism, String startdate, String email, String aTitle){
		super(name, gender, dob, address, postcode, nin, specialism, startdate, email);
		this.setTitle(aTitle); //Title is specific to the Consultant class
		   
			}
    /*
     * Read the title of a consultant
     * @return Returns the value of a consultant's title
     */
	public String getTitle() { 
		return title; 
	}
    /* 
     * The method allows to write a title
     */
	public void setTitle(String title) { 
		this.title = title; 
	} 
	
	/*@return This method gets the consultants's information*/
	public String toString(){
		return " Name "+super.getName()+" Gender "+super.getGender()+" Dob "+super.getDob()+" Address "+super.getAddress()+" Postcode "+super.getPostcode()+" NIN "+super.getNIN()+" Specialism "+super.getSpecialism()+" StartDate "+super.getStartDate()+" Email "+super.getEmail()+" Title "+this.title;
	}
	/*
	 * Create an empty array list, populate with data using 'add', remove and display data 
	 * @return Returns the list of consultants 
	 */
	public void consultantdoc(){
		//Create an empty ArrayList of Consultants
		ArrayList<Consultant> consultantlist=new ArrayList<Consultant>();
		     
		    //The method "add" allows to add values in the consultantlist
			consultantlist.add(new Consultant("Lisa Amor","F","06.09.1976","12 Cambridge Road","675467","LA 47 87 33 L","Allergist","11.05.2010","lisamor@mail.com","Allergist"));
			consultantlist.add(new Consultant("Holly Clark","F","18.09.1980","68 Oxford Road","567845","HC 65 34 67 H","Dentist","08.07.2010","hollyclark@mail.com","Dentist"));
		    
			//Show number of consultants in a list
			System.out.println("No of Doctors in a consultant list: "+consultantlist.size());
			//Display consultants
			for (Consultant con:consultantlist){
				System.out.println(con);
			}
		    //"Remove" method removes a consultant at index 12
			//consultantlist.remove(12);
	} 
}
