/*
 * This class stores information about Persons
 * @author Aliya Tulyakova, ID 16035926
 * @version 27.10.2016
 * 
 * 
 */

public class Person {
	/*Creates attributes*/
		private String Name;
		private String Gender;
		private String Dob;
		private String Address;
		private String Postcode;
		private String NIN;
		
		/*Person Constructor initialise Name, Gender, Dob, Address, Postcode, NIN to parameters in the constructor
		 * 
		 * @param Name - describes a name of a person
		 * @param Gender - describes a gender of a person
		 * @param Dob - describes a date of birth of a person
		 * @param Address - describes an address of a person
		 * @param Postcode - describes a postcode of a person
		 * @param NIN - describes a National Insurance Number of a person
		 * */
		public Person(String Name, String Gender, String Dob, String Address, String Postcode, String NIN){
			this.Name=Name;
			this.Gender=Gender;
			this.Dob=Dob;
			this.Address=Address;
			this.Postcode=Postcode;
			this.NIN=NIN;
			
		}
		
		/*Creates methods*/
		
        /*Read the name of a person
         * @return Returns the value of a person's name 
         * */
		public String getName() { 
			return Name;    
		}
		/*
		 * The method allows to write a Name 
		 */
		public void setName(String Name) { 
			this.Name = Name;
		}
		/*
		 * Read the gender of a person
		 * @return Returns the value of a person's gender
		 */
		public String getGender() { 
			return Gender; 
		}
		/*
		 * The method allows to write a Gender
		 */
		public void setGender(String Gender) { 
			this.Gender = Gender;
			
		}
		/*
		 * Read the date of birth of a person
		 * @return Returns the value of a person's date of birth
		*/
		public String getDob() { 
			return Dob; 
		}
		/*
		 * The method allows to write a date of birth
		 */
		public void setDob(String Dob) { 
			this.Dob = Dob;
		}
		/*
		 * Read the address of a person
		 * @return Returns the value of a person's address
		 */
		public String getAddress() { 
			return Address; 
		}
		/*
		 * The method allows to write an address
		 */
		public void setAddress(String Address) { 
			this.Address = Address;
		}
		/*
		 * Read the postcode of a person
		 * @return Returns the value of a person's postcode
		 */
		public String getPostcode() { 
			return Postcode; 
		}
		/*
		 * The method allows to write a postcode
		 */
		public void setPostcode(String Postcode) { 
			this.Postcode = Postcode;
		}
		/*
		 * Read the NIN of a person
		 * @return Returns the value of a person's NIN
		 */
		public String getNIN() { 
			return NIN; 
		}
		/*
		 * The method allows to write NIN
		 */
		public void setNIN(String NIN) { 
			this.NIN = NIN;
		}
		/*@return This method gets person's information*/
		  public String toString(){
			  return "Name: "+getName()+" Gender :"+getGender()+" DOB: "+getDob()+" Address "+getAddress()+" Postcode "+getPostcode()+" NIN "+getNIN();
			  }
		
	}


