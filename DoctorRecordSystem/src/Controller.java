import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;

/*
 * This is the main class. It allows to create new "Doctors" and displays all data
 * @author Aliya Tulyakova, ID 16035926
 * @version 27.10.2016
 */
public class Controller {
	public static void main(String[] args) throws SQLException {
		final DoctorDAO dao=new DoctorDAO();
		/*Create new ArrayList
		 * @param doctorList - ArrayList<Doctor>
		 * */
		ArrayList<Doctor> doctorList=new ArrayList<Doctor>();
		
		/*Print all employees using DAO SelectAllEmployees*/
	if(doctorList!=null)
		doctorList=dao.selectAllEmployees();
	    
	for (Doctor c:doctorList) {
		System.out.println(c);
	}
	
	/*Create new Consultant and get consultantdoc() method to add, display and delete
	 * @param con - new Consultant
	 *  */
	System.out.println("Consultants list:");
	Consultant con=new Consultant("","","","","","","","","",""); //Creates an instance
	con.consultantdoc();
	
	dao.selectEmployeeByName(); //get selectEmployeeByName() method from DoctorDAO to select an employee by name
	dao.deleteDoctorById(); //get deleteDoctorById() method from DoctorDAO to delete a doctor by entering their id in a console
	dao.retrieveafterdel(); //get retrieveafterdel() method from DoctorDAO to 'update' the database after delete statement and retrieve employees
	
	//Test employee insertion using DAO insertEmployee
	/*
	Doctor doctor=new Doctor();
	doctor.setName("Aryeh Rubinstein");
	doctor.setGender("M");
	doctor.setDob("21.07.1981");
	doctor.setAddress("3 Beech Street");
	doctor.setPostcode("M261GH");
	doctor.setNIN("AR 1345324 A");
	doctor.setSpecialism("Heart");
	doctor.setStartDate("22th July 2014");
	doctor.setEmail("aryehr@mail.com");
	
	//Print all employees using DAO SelectAllEmployess
	if (doctorList!=null)
		doctorList=dao.selectAllEmployees();
	for (Doctor c:doctorList){
		System.out.println(c);
	} */
	
	//Develop front end application
	 //GET operation
	try {
		HttpServer server=HttpServer.create(new InetSocketAddress(8001),0); //creates server
		//list employees
	server.createContext("/", new HttpHandler() {
		/* Creates front-end to get data from doctordb database
		 * @throws IOException
		 * */
		@Override
	public void handle(HttpExchange he) throws IOException{
		final String head="<html><head><style> tr:nth-child(even) {background-color:#f2f2f2;} th{background-color: #4CAF50; color: white;} th, td{ border-bottom: 1px solid #ddd;}</style></head><body><table><tr><th>Name</th><th><Specialism</th><th><Action</th></tr>";	
		final String foot="</table></body></html>";
		BufferedWriter out=new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
		ArrayList<Doctor> doctors=dao.selectInfo();
		he.sendResponseHeaders(200, 0); //send http response, otherwise not work  
		out.write(head);
		
		for(Doctor c:doctors) {
			out.write( 
					"<tr><td>"+c.getName()+
					
					"</td><td>"+c.getSpecialism()+
					
			        "</td><td>"+ "<a href=/viewmoreinfo>View</a>");
		}
		out.write(foot);
		out.close();
		}
	});
	//GET more information
	server.createContext("/viewmoreinfo", new HttpHandler() {
		@Override
		/*Create front-end to get the rest of the date of employees after pressing 'view' in a table at 'localhost:8001/' */
	public void handle(HttpExchange he) throws IOException{
		final String head="<html><head><style> tr:nth-child(even) {background-color:#f2f2f2;} th{background-color: #4CAF50; color: white;} th, td{ border-bottom: 1px solid #ddd;}</style></head><body><table><tr><th>Name</th><th>Gender</th><th>Dob</th><th>Address</th><th>Postcode</th><th>NIN</th><th>StartDate</th><th>Email</th></tr>";	
		final String foot="</table></body></html>";
		BufferedWriter out=new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
		ArrayList<Doctor> doctors=dao.selectMoreInfo();
		he.sendResponseHeaders(200, 0); //send http response, otherwise not work 
		out.write(head);
		
		for(Doctor c:doctors) {
			out.write( 
					"<tr><td>"+c.getName()+
					"</td><td>"+c.getGender()+
					"</td><td>"+c.getDob()+
					"</td><td>"+c.getAddress()+
					"</td><td>"+c.getPostcode()+
					"</td><td>"+c.getNIN()+
					"</td><td>"+c.getStartDate()+
					"</td><td>"+c.getEmail());
		}
		out.write(foot);
		out.close();
		}
	});
	
	//POST operation
	
	server.createContext("/post", new HttpHandler() {
	@Override
	/*
	 * Creates front-end to insert data into a database using a form
	 * @throws IOException
	 */
	public void handle (HttpExchange he) throws IOException {
	
		//output HTML form
	he.sendResponseHeaders(200, 0);	
	BufferedWriter out=new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
	out.write("<html><head><style>input[type=submit]{ width: 30%; background-color: #4CAF50; color: white; padding: 14px 20px; margin: 8px 0; border: none; border-radius: 4px; cursor: pointer;} input[type=submit]:hover{ background-color: #45a049;} input[type=text]{ width: 20%; padding: 12px 20px; margin: 8px 0; display: inline-block; border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box;} div{ border-radius: 5px; background-color: #f2f2f2; padding: 20px;} :required:focus{ box-shadow: 0 0 3px rgba(255,0,0,0.5);}</style></head><body><div><form method=\"POST\" action=\"/add_write\">");
	out.write("Name:</br><input required name=\"Name\"><br>");
	out.write("Gender:</br><input name=\"Gender\"><br>");
	out.write("Dob:</br><input name=\"Dob\"><br>");
	out.write("Address:</br><input name=\"Address\"><br>");
	out.write("Postcode:</br><input name=\"Postcode\"><br>");
	out.write("NIN:</br><input name=\"NIN\"><br>");
	out.write("Specialism:</br><input required name=\"Specialism\"><br>");
	out.write("Start Date:</br><input required name=\"StartDate\"><br>");
	out.write("Email:</br><input required name=\"Email\"><br>");
	out.write("<input type=\"submit\" value=\"Submit\">");
	out.write("</div></form></body></html>");
	out.close();
	}
	});
	
	server.createContext("/add_write", new HttpHandler() {
	//process data from /add form
	@Override
	public void handle (HttpExchange he) throws IOException {
	HashMap<String, String> post=new HashMap<String,String>();	
	 //read the request body
	BufferedReader in=new BufferedReader(new InputStreamReader(he.getRequestBody()));
	String line="";
	String request="";
	while ((line=in.readLine())!=null) {
		request=request+line;
	}
	//individual key=value pairs are delimited by ampersands. Tokenize.
	String[] pairs=request.split("&");
	for (int i=0;i<pairs.length;i++) {
		//each key=value pair is separated by an equals, and both halves require URL decoding.
		String pair=pairs[i];
		post.put(URLDecoder.decode(pair.split("=")[0],"UTF-8"), URLDecoder.decode(pair.split("=")[1], "UTF-8"));
	}
	//Should have a HashMap of posted data in our "post" variable. Now to add a contact
	Doctor c=new Doctor("","","","","","","","","");
	c.setName(post.get("Name"));
	c.setGender(post.get("Gender"));
	c.setDob(post.get("Dob"));
	c.setAddress(post.get("Address"));
	c.setPostcode(post.get("Postcode"));
	c.setNIN(post.get("NIN"));
	c.setSpecialism(post.get("Specialism"));
	c.setStartDate(post.get("StartDate"));
	c.setEmail(post.get("Email"));
	
	BufferedWriter out=new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
	try{
		dao.insertEmployee(c);
		he.sendResponseHeaders(200, 0);
		//HTTP 200 (OK)
		out.write("Success!");
	}
	catch (SQLException se) {
		he.sendResponseHeaders(500, 0);
		//HTTP 500 (Internal Server Error)
		out.write("Error Adding Employee");
	}
	}
	});
	
	//actually start the server
	server.start();
	} catch (IOException ioe){
	System.err.println("IOException "+ioe.getMessage()+" "+ioe.getStackTrace());
	}
	}}