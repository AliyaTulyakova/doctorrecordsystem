DROP TABLE IF EXISTS "doctors";
CREATE TABLE "doctors" ("ID" INTEGER PRIMARY KEY  NOT NULL ,"Name" VARCHAR(60) NOT NULL ,"Gender" VARCHAR(2) DEFAULT (null) ,"DOB" VARCHAR(20),"Address" VARCHAR(100),"Postcode" VARCHAR(10),"NIN" VARCHAR(10),"Specialism" VARCHAR(45),"StartDate" VARCHAR(14),"Email" VARCHAR(60));
INSERT INTO "doctors" VALUES(1,'Olivia Roberts','F','12.10.1981','112 Princess Street','123456','OR 11 23 45 O','Pediatrician','03th June 2008','oliviarob@mail.com');
INSERT INTO "doctors" VALUES(2,'Maria Dallas','F','03.09.1981','11 Cross Street','567456','MD 45 67 87 M','Surgeon','11th September 2009','dallasmaria@mail.com');
INSERT INTO "doctors" VALUES(3,'Emma Smith','F','02.12.1988','14 Cambridge Street','678456','ES 34 56 87 E','Pediatrician','01st November 2013','emmasmith@mail.com');
